#!/bin/bash

function pull_aurora() {
  mkdir -p priv-app/AuroraServices
  mkdir -p etc/permissions

  wget -q --show-progress \
    https://gitlab.com/AuroraOSS/AuroraServices/uploads/c22e95975571e9db143567690777a56e/AuroraServices_v1.1.1.apk \
    -O priv-app/AuroraServices/AuroraServices.apk

  wget -q --show-progress \
    https://gitlab.com/AuroraOSS/AuroraServices/raw/master/app/src/main/assets/permissions_com.aurora.services.xml \
    -O etc/permissions/privapp-permissions-com.aurora.services.xml
}

function pull_bromite() {
  mkdir -p app/{ChromePublic,SystemWebView}/{arm,arm64}

  url_stem="https://github.com/bromite/bromite/releases/download"
  latest_tag=$(curl -s https://api.github.com/repos/bromite/bromite/releases/latest | jq -r '.tag_name')

  wget -q --show-progress ${url_stem}/${latest_tag}/arm_ChromePublic.apk -O app/ChromePublic/arm/ChromePublic.apk
  wget -q --show-progress ${url_stem}/${latest_tag}/arm_SystemWebView.apk -O app/SystemWebView/arm/SystemWebView.apk
  wget -q --show-progress ${url_stem}/${latest_tag}/arm64_ChromePublic.apk -O app/ChromePublic/arm64/ChromePublic.apk
  wget -q --show-progress ${url_stem}/${latest_tag}/arm64_SystemWebView.apk -O app/SystemWebView/arm64/SystemWebView.apk
}

pull_aurora
pull_bromite
