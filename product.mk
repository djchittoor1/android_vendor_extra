# ADB keys
PRODUCT_COPY_FILES += \
    vendor/extra/adb_keys:$(TARGET_RECOVERY_ROOT_OUT)/root/adb_keys \
    vendor/extra/adb_keys:$(TARGET_ROOT_OUT)/adb_keys

# Audio
$(call inherit-product, vendor/extra/audio/audio.mk)

# Default ADB shell prompt
PRODUCT_SYSTEM_DEFAULT_PROPERTIES += \
    persist.sys.adb.shell=/system/xbin/bash

# Disable RescueParty due to high risk of data loss
PRODUCT_PRODUCT_PROPERTIES += \
    persist.sys.disable_rescue=true

# Enable blurs, hidden under dev option
PRODUCT_PRODUCT_PROPERTIES += \
    ro.surface_flinger.supports_background_blur=1 \
    persist.sys.sf.disable_blurs=1 \
    ro.sf.blurs_are_expensive=1

# fastbootd
PRODUCT_PACKAGES += fastbootd
PRODUCT_DEFAULT_PROPERTY_OVERRIDES += ro.fastbootd.available=true

# Framework overlay needs to be a RRO
PRODUCT_ENFORCE_RRO_TARGETS += framework-res

# GApps
ifeq ($(WITH_GMS), true)
$(call inherit-product, vendor/gms/products/gms.mk)
endif # WITH_GMS

# GCam - Greatness
$(call inherit-product-if-exists, packages/apps/GoogleCamera/config.mk)

# ih8sn
PRODUCT_PACKAGES += ih8sn

ifneq ("$(wildcard  vendor/extra/configs/ih8sn/ih8sn_$(subst lineage_,,$(TARGET_PRODUCT)).conf)","")
PRODUCT_COPY_FILES += \
    vendor/extra/configs/ih8sn/ih8sn_$(subst lineage_,,$(TARGET_PRODUCT)).conf:/system/etc/ih8sn.conf
else
PRODUCT_COPY_FILES += \
    vendor/extra/configs/ih8sn/ih8sn_generic.conf:/system/etc/ih8sn.conf
endif

# Overlays
PRODUCT_PACKAGE_OVERLAYS += vendor/extra/overlay

# Prebuilts
PRODUCT_PACKAGES += \
    AuroraServices \
    ChromePublic \
    SystemWebView

# Recovery
PRODUCT_SYSTEM_DEFAULT_PROPERTIES += persist.sys.recovery_update=true

# Tinymix
PRODUCT_PACKAGES += tinymix

# Updater
PRODUCT_SYSTEM_DEFAULT_PROPERTIES += lineage.updater.allow_downgrading=true
PRODUCT_SYSTEM_DEFAULT_PROPERTIES += lineage.updater.uri="https://dl.keksla.wtf/OTA/lineage-18.1/{device}.json"

# Use 64-bit dex2oat for better dexopt time
ifeq ($(TARGET_SUPPORTS_64_BIT_APPS), true)
PRODUCT_PROPERTY_OVERRIDES += \
    dalvik.vm.dex2oat64.enabled=true
endif
